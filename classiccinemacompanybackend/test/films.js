const mongoose = require(`mongoose`);
const Film = require('../models/film.model');

const chai = require(`chai`);
const chaiHttp = require(`chai-http`);
const should = chai.should();
const expect = require(`chai`).expect;
const server = require(`../server`);

const testData = require(`./testData/sampleFilms`);

const TESTBASEPATH = `/allFilms`;

chai.use(chaiHttp);

describe(`Test of retrieving films film`, () => {
    before(done => {
        Film.deleteMany({}, err => {
            if (err) { done(); }
        });

        Film.insertMany(testData.films, (err, res) => {
            if (err) {
                console.log(`Error inserting`);
                done();
            } else {
                console.log(`Documents inserted`);
                done();
            }
        });
    });

    describe(`Test allFilms methods`, () => {
        const TESTBASEPATH = `/allFilms`;

        it(`should retrieve all of the films when /allFilms is hit`, done => {
            chai.request(server)
                .get(TESTBASEPATH)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.an('array');
                    res.body.length.should.eql(3);
                    done();
                });
        });

        it(`should retrieve all films with status 1 when /allFilms/1 is hit`, done => {
            chai.request(server)
                .get(`${TESTBASEPATH}/1`)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.an('array');
                    res.body.length.should.eql(2);
                    done();
                });
        });

        it(`should retrieve all films with a status if 2 when /allFilms/2 is hit`, done => {
            chai.request(server)
                .get(`${TESTBASEPATH}/2`)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.an('array');
                    res.body.length.should.eql(1);
                    done();
                });
        });

        it(`should return a 422 status when anything other than 1 or 2 is supplied to allFilms/:status`, done => {
            chai.request(server)
                .get(`${TESTBASEPATH}/not1or2`)
                .end((err, res) => {
                    res.should.have.status(422);
                    res.body.should.be.an('object');
                    res.body.should.have.property('message', `Not a valid film status`);
                    done();
                });
        });

        it(`should return a 422 status when a number not 1 or 2 is supplied to allFilms/:status`, done => {
            chai.request(server)
                .get(`${TESTBASEPATH}/4`)
                .end((err, res) => {
                    res.should.have.status(422);
                    res.body.should.be.an('object');
                    res.body.should.have.property('message', `Not a valid film status`);
                    done();
                });
        });
    });

    describe(`Alternate data for this test required`, () => {
        beforeEach(done => {
            Film.deleteMany({}, err => {
            });

            Film.insertMany(testData.films2, (err, res) => {
                if (err) {
                    console.log(`Error inserting`);
                } else {
                    console.log(`Documents inserted - no status 2`);
                }
            });
            done();
        });

        it(`should return a 404 when no films with a valid status are found`, done => {

            chai.request(server)
                .get(`${TESTBASEPATH}/2`)
                .end((err, res) => {
                    res.should.have.status(404);
                    res.body.should.be.an('object');
                    res.body.should.have.property(`message`, `No films found`);
                    done();
                });
        });
    });

    describe(`Test singleFilms methods`, () => {
        const TESTBASEPATH = `/singleFilm`;
        it(`should retrieve the film with the given id when /singleFilm/:id is hit`, done => {
            chai.request(server)
                .get(`${TESTBASEPATH}/${testData.films[1][`_id`]}`)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.an('object');
                    res.body.should.have.property(`_id`, testData.films[1][`_id`]);
                    done();

                });
        });

        it(`should return a 422 when the film id asked for is not in the correct format`, done => {
            chai.request(server)
                .get(`${TESTBASEPATH}/notAValidId`)
                .end((err, res) => {
                    res.should.have.status(422);
                    res.body.should.have.property(`message`, `There was a problem with the film Id`);
                    done();
                });
        });

        it(`should return a 404 when the film id asked for is valid but not found`, done => {
            chai.request(server)
                .get(`${TESTBASEPATH}/123456789012345678901234`)
                .end((err, res) => {
                    res.should.have.status(404);
                    res.body.should.have.property(`message`, `Not found`);
                    done();
                });
        });
    });
});
const mongoose = require(`mongoose`);
const Schema = mongoose.Schema;
//const { hhmmRegExp } = require(`../js/regExps`);

const OpeningTime = new Schema({
  days: { type: String, required: true },
  times: {
    type: String,
    required: true /*, match: [hhmmRegExp, `is invalid`] */
  }
  //   close: {
  //     type: String,
  //     required: true /*, match: [hhmmRegExp, `is invalid`] */
  //   }
});

module.exports = mongoose.model(`OpeningTime`, OpeningTime);

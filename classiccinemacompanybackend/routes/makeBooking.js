const express = require(`express`);
const router = express.Router();
const Booking = require(`../models/booking.model`);
const bodyParser = require(`body-parser`);

const { check, validationResult } = require(`express-validator/check`);

const { isoDateRegExp, emailRegExp } = require(`../js/regExps`);

router.use(bodyParser.json());

router.route(`/`).post(
    [
        check('filmId').exists().isMongoId(),
        check('bookingDate').exists().matches(isoDateRegExp),
        check('email').exists().isEmail(),
        check('adults').optional().isInt({ gt: -1 }),
        check('child').optional().isInt({ gt: -1 }),
        check('concessions').optional().isInt({ gt: -1 })
    ],
    (req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({
                'message': `There were errors in the booking data`,
                'error': errors.array()
            });
        }
        const booking = new Booking(req.body);
        booking.save()
            .then(booking => {
                res.status(200).json({ 'message': `Booking successful` });
            })
            .catch(err => {
                res.status(400).json({
                    'message': `Booking not made`,
                    'error': err
                });
            });
    }
);

module.exports = router;
const express = require(`express`);
const router = express.Router();
const Signup = require(`../models/signup.model`);
const bodyParser = require(`body-parser`);

const { check, validationResult } = require(`express-validator/check`);

const { isoDateRegExp, emailRegExp, phoneNumberRegExp } = require(`../js/regExps`);
const { validTitles, validGenders } = require(`../js/validData`);

router.use(bodyParser.json());

router.route(`/`).post(
    [
        check('title').exists().isIn(validTitles),
        check('firstName').exists().isLength({ min: 2 }),
        check('lastName').exists().isLength({ min: 2 }),
        check('email').exists().isEmail(),
        check('phoneNumber').optional({ checkFalsy: true }).matches(phoneNumberRegExp),
        check('dob').optional({ checkFalsy: true }).matches(isoDateRegExp),
        check('gender').optional({ checkFalsy: true }).isIn(validGenders)
    ],
    (req, res) => {
        console.log(req.body);
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({
                'message': `There were errors in the sign up data`,
                'error': errors.array()
            });
        }
        const signup = new Signup(req.body);
        signup.save()
            .then(signup => {
                res.status(200).json({ 'message': `Sign up successful` });
            })
            .catch(err => {
                res.status(400).json({
                    'message': `Sign up unsuccessful`,
                    'error': err
                });
            });
    }
);

module.exports = router;
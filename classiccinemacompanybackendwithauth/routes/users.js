const express = require(`express`);
const router = express.Router();
const User = require(`../models/user.model`);
const bodyParser = require(`body-parser`);
const passport = require("passport");
const { SECRET, ALGORITHM } = require('../config/config.json');
const jwt = require('jsonwebtoken');

const { check, validationResult } = require(`express-validator/check`);

const { phoneNumberRegExp } = require(`../js/regExps`);
const { validTitles, validGenders } = require(`../js/validData`);

router.use(bodyParser.json());

router.route(`/register`).post(
    [
        check('username').exists().isLength({ min: 2 }),
        check('password').exists().isLength({ min: 2 }),
        check('title').exists().isIn(validTitles),
        check('firstName').exists().isLength({ min: 2 }),
        check('lastName').exists().isLength({ min: 2 }),
        check('email').exists().isEmail(),
        check('phoneNumber').optional({ checkFalsy: true }).matches(phoneNumberRegExp),
        check('dob').optional({ checkFalsy: true }).isISO8601(),
        check('gender').optional({ checkFalsy: true }).isIn(validGenders)
    ],
    (req, res) => {
        console.log(req.body);
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({
                'message': `There were errors in the sign up data`,
                'error': errors.array()
            });
        }
        const { password, ...userDetails } = req.body;
        User.register(new User(userDetails), password, function (err, user) {
            if (err) {
                return res.status(500).send(err.message);
            }

            passport.authenticate('local')(req, res, function () {
                const token = jwt.sign({ sub: req.user.id }, SECRET, { algorithm: ALGORITHM });
                res.status(201).send({ greeting: 'hello ' + req.user.username, token: token });
            });
        });
    }
);

router.post("/login", passport.authenticate("local"), (req, res) => {
    const token = jwt.sign({ sub: req.user.id }, SECRET, { algorithm: ALGORITHM });
    res.send(token);
});

router.post(`/update`, passport.authenticate("jwt"),
    [
        check('title').exists().isIn(validTitles),
        check('firstName').exists().isLength({ min: 2 }),
        check('lastName').exists().isLength({ min: 2 }),
        check('email').exists().isEmail(),
        check('phoneNumber').optional({ checkFalsy: true }).matches(phoneNumberRegExp),
        check('dob').optional({ checkFalsy: true }).isISO8601(),
        check('gender').optional({ checkFalsy: true }).isIn(validGenders)
    ],
    (req, res) => {
        console.log(req.body);
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({
                'message': `There were errors in the profile data`,
                'error': errors.array()
            });
        }
        const { user } = req;
        User.findByIdAndUpdate(user.id, req.body).then(user => {
            res.status(200).json({ 'message': `Update successful`, 'id': user.id });
        })
            .catch(err => {
                res.status(400).json({
                    'message': `Update unsuccessful`,
                    'error': err
                });
            });
    }
);

router.get("/", passport.authenticate("jwt"), (req, res) => {
    const { user } = req;
    if (user) {
        res.json(user);
    } else {
        res.status(500).json({
            message: "Current user not found"
        })
    }
});

module.exports = router;
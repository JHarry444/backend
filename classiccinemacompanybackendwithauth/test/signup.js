const mongoose = require(`mongoose`);
const Signup = require(`../models/signup.model`);

const chai = require(`chai`);
const chaiHttp = require(`chai-http`);
const should = chai.should();
const server = require(`../server`);
const testData = require(`./testData/sampleSignup`);

const TESTPATH = `/signup`;

chai.use(chaiHttp);

describe(`Test sign up system`, () => {
    beforeEach(done => {
        Signup.deleteMany({}, err => {
            done();
        });
    });

    describe(`Test POST data required errors`, () => {
        it(`should not create a signup without a title field`, done => {
            const signup = testData.noTitle;
            chai.request(server)
                .post(TESTPATH)
                .send(signup)
                .end((err, res) => {
                    res.should.have.status(422);
                    res.body.should.have.property(`error`);
                    res.body.should.have.property(`message`, `There were errors in the sign up data`);
                    done();
                });
        });

        it(`should not create a signup without a firstName field`, done => {
            const signup = testData.noFirstName;
            chai.request(server)
                .post(TESTPATH)
                .send(signup)
                .end((err, res) => {
                    res.should.have.status(422);
                    res.body.should.have.property(`error`);
                    res.body.should.have.property(`message`, `There were errors in the sign up data`);
                    done();
                });
        });

        it(`should not create a signup without a lastName field`, done => {
            const signup = testData.noLastName;
            chai.request(server)
                .post(TESTPATH)
                .send(signup)
                .end((err, res) => {
                    res.should.have.status(422);
                    res.body.should.have.property(`error`);
                    res.body.should.have.property(`message`, `There were errors in the sign up data`);
                    done();
                });
        });

        it(`should not create a signup without an Email field`, done => {
            const signup = testData.noEmail;
            chai.request(server)
                .post(TESTPATH)
                .send(signup)
                .end((err, res) => {
                    res.should.have.status(422);
                    res.body.should.have.property(`error`);
                    res.body.should.have.property(`message`, `There were errors in the sign up data`);
                    done();
                });
        });

        it(`should create a signup if only title, firstName, lastName and email are valid and present`, done => {
            const signup = new Signup(testData.onlyRequired);
            chai.request(server)
                .post(TESTPATH)
                .send(signup)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.property(`message`, `Sign up successful`);
                    done();
                });
        });
    });

    describe(`Test POST validity`, () => {
        describe(`Title Validity tests:`, () => {
            it(`should reject an invalid title`, done => {
                const signup = testData.invalidTitle;
                chai.request(server)
                    .post(TESTPATH)
                    .send(signup)
                    .end((err, res) => {
                        res.should.have.status(422);
                        res.body.should.have.property(`error`);
                        res.body.should.have.property(`message`, `There were errors in the sign up data`);
                        done();
                    });
            });

            it(`should accept a valid title`, done => {
                const signup = testData.randomValidTitle();
                chai.request(server)
                    .post(TESTPATH)
                    .send(signup)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property(`message`, `Sign up successful`);
                        done();
                    });
            });
        });

        describe(`FirstName Validity tests:`, () => {
            it(`should reject a firstName of less than 2 chars`, done => {
                const signup = testData.invalidFirstName;
                chai.request(server)
                    .post(TESTPATH)
                    .send(signup)
                    .end((err, res) => {
                        res.should.have.status(422);
                        res.body.should.have.property(`error`);
                        res.body.should.have.property(`message`, `There were errors in the sign up data`);
                        done();
                    });
            });

            it(`should accept a valid firstName`, done => {
                const signup = testData.randomValidFirstName();
                chai.request(server)
                    .post(TESTPATH)
                    .send(signup)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property(`message`, `Sign up successful`);
                        done();
                    });
            });
        });

        describe(`LastName Validity tests:`, () => {
            it(`should reject a lastName of less than 2 chars`, done => {
                const signup = testData.invalidLastName;
                chai.request(server)
                    .post(TESTPATH)
                    .send(signup)
                    .end((err, res) => {
                        res.should.have.status(422);
                        res.body.should.have.property(`error`);
                        res.body.should.have.property(`message`, `There were errors in the sign up data`);
                        done();
                    });
            });

            it(`should accept a valid lastName`, done => {
                const signup = testData.randomValidLastName();
                chai.request(server)
                    .post(TESTPATH)
                    .send(signup)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property(`message`, `Sign up successful`);
                        done();
                    });
            });
        });

        describe(`Email Validity tests:`, () => {
            it(`should reject an email not in the specified format`, done => {
                const signup = testData.invalidEmail;
                chai.request(server)
                    .post(TESTPATH)
                    .send(signup)
                    .end((err, res) => {
                        res.should.have.status(422);
                        res.body.should.have.property(`error`);
                        res.body.should.have.property(`message`, `There were errors in the sign up data`);
                        done();
                    });
            });

            it(`should accept a valid lastName`, done => {
                const signup = testData.randomValidEmail();
                chai.request(server)
                    .post(TESTPATH)
                    .send(signup)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property(`message`, `Sign up successful`);
                        done();
                    });
            });
        });

        describe(`PhoneNumber Validity tests:`, () => {
            it(`should reject a phoneNumber in an invalid format if present`, done => {
                const signup = testData.invalidPhoneNumber;
                chai.request(server)
                    .post(TESTPATH)
                    .send(signup)
                    .end((err, res) => {
                        res.should.have.status(422);
                        res.body.should.have.property(`error`);
                        res.body.should.have.property(`message`, `There were errors in the sign up data`);
                        done();
                    });
            });

                it(`should accept a valid phoneNumber if present`, done => {
                    const signup = testData.randomValidPhoneNumber();
                    chai.request(server)
                        .post(TESTPATH)
                        .send(signup)
                        .end((err, res) => {
                            res.should.have.status(200);
                            res.body.should.have.property(`message`, `Sign up successful`);
                            done();
                        });
                });
        });

        describe(`Dob Validity tests:`, () => {
            it(`should reject a dob in an invalid format if present`, done => {
                const signup = testData.invalidDob;
                chai.request(server)
                    .post(TESTPATH)
                    .send(signup)
                    .end((err, res) => {
                        res.should.have.status(422);
                        res.body.should.have.property(`error`);
                        res.body.should.have.property(`message`, `There were errors in the sign up data`);
                        done();
                    });
            });

            it(`should accept a valid dob if present`, done => {
                const signup = testData.randomValidDob();
                chai.request(server)
                    .post(TESTPATH)
                    .send(signup)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property(`message`, `Sign up successful`);
                        done();
                    });
            });
        });

        describe(`Gender Validity tests:`, () => {
            it(`should reject a gender that is not female or male`, done => {
                const signup = testData.invalidGender;
                chai.request(server)
                    .post(TESTPATH)
                    .send(signup)
                    .end((err, res) => {
                        res.should.have.status(422);
                        res.body.should.have.property(`error`);
                        res.body.should.have.property(`message`, `There were errors in the sign up data`);
                        done();
                    });
            });

            it(`should accept a valid gender if present`, done => {
                const signup = testData.randomValidGender();
                chai.request(server)
                    .post(TESTPATH)
                    .send(signup)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property(`message`, `Sign up successful`);
                        done();
                    });
            });
        });

        it(`should accept valid signup data if all are present`, done => {
            const signup = testData.randomValidSignup();
            chai.request(server)
                .post(TESTPATH)
                .send(signup)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.property(`message`, `Sign up successful`);
                    done();
                });
        });
    });
});
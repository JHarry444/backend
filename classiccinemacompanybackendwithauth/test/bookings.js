const mongoose = require(`mongoose`);
const Booking = require('../models/booking.model');

const chai = require(`chai`);
const chaiHttp = require(`chai-http`);
const should = chai.should();
const server = require(`../server`);

const testData = require(`./testData/sampleBookings`);

const TESTPATH = `/makeBooking`;

chai.use(chaiHttp);

describe(`Test of booking system`, () => {
    beforeEach(done => {
        Booking.deleteMany({}, err => {
            console.log(`All records deleted`);
            done();
        });
    });

    describe(`Test filmId POST errors`, done => {
        it(`should not create a booking without a filmId field`, done => {
            let booking = testData.noFilmId;
            chai.request(server)
                .post(TESTPATH)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(422);
                    res.body.should.have.property('error');
                    res.body.should.have.property('message', `There were errors in the booking data`);
                    done();
                });
        });

        it(`should not create a booking if the filmId field is not of type MongoId`, done => {
            let booking = testData.incorrectFilmIdFormat;
            chai.request(server)
                .post(TESTPATH)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(422);
                    res.body.should.have.property('error');
                    res.body.should.have.property('message', `There were errors in the booking data`);
                    done();
                });
        });
    });

    describe(`Test bookingDate POST errors`, done => {
        it(`should not create a booking without a bookingDate field`, done => {
            let booking = testData.noBookingDate;
            chai.request(server)
                .post(TESTPATH)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(422);
                    res.body.should.have.property('error');
                    res.body.should.have.property('message', `There were errors in the booking data`);
                    done();
                });
        });

        it(`should not create a booking if the bookingDate field does not pass the regular expression for ISO Date`, done => {
            let booking = testData.incorrectBookingDateFormat;
            chai.request(server)
                .post(TESTPATH)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(422);
                    res.body.should.have.property('error');
                    res.body.should.have.property('message', `There were errors in the booking data`);
                    done();
                });
        });
    });

    describe(`Test email POST errors`, done => {
        it(`should not create a booking without an email field`, done => {
            let booking = testData.noEmail;
            chai.request(server)
                .post(TESTPATH)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(422);
                    res.body.should.have.property('error');
                    res.body.should.have.property('message', `There were errors in the booking data`);
                    done();
                });
        });

        it(`should not create a booking if the filmId field is not of type MongoId`, done => {
            let booking = testData.incorrectEmailFormat;
            chai.request(server)
                .post(TESTPATH)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(422);
                    res.body.should.have.property('error');
                    res.body.should.have.property('message', `There were errors in the booking data`);
                    done();
                });
        });
    });

    describe(`Test adult POST errors`, done => {
        it(`should not create a booking if the adult ticket field is less than 0`, done => {
            let booking = testData.negativeAdultTickets;
            chai.request(server)
                .post(TESTPATH)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(422);
                    res.body.should.have.property('error');
                    res.body.should.have.property('message', `There were errors in the booking data`);
                    done();
                });
        });

        it(`should not create a booking if the adult ticket field is not an integer value`, done => {
            let booking = testData.nonIntegerAdultTickets;
            chai.request(server)
                .post(TESTPATH)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(422);
                    res.body.should.have.property('error');
                    res.body.should.have.property('message', `There were errors in the booking data`);
                    done();
                });
        });
    });

    describe(`Test child POST errors`, done => {
        it(`should not create a booking if the child ticket field is less than 0`, done => {
            let booking = testData.negativeChildTickets;
            chai.request(server)
                .post(TESTPATH)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(422);
                    res.body.should.have.property('error');
                    res.body.should.have.property('message', `There were errors in the booking data`);
                    done();
                });
        });

        it(`should not create a booking if the child ticket field is not an integer value`, done => {
            let booking = testData.nonIntegerChildTickets;
            chai.request(server)
                .post(TESTPATH)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(422);
                    res.body.should.have.property('error');
                    res.body.should.have.property('message', `There were errors in the booking data`);
                    done();
                });
        });
    });

    describe(`Test concessions POST errors`, done => {
        it(`should not create a booking if the concessions ticket field is less than 0`, done => {
            let booking = testData.negativeConcessionsTickets;
            chai.request(server)
                .post(TESTPATH)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(422);
                    res.body.should.have.property('error');
                    res.body.should.have.property('message', `There were errors in the booking data`);
                    done();
                });
        });

        it(`should not create a booking if the concessions ticket field is not an integer value`, done => {
            let booking = testData.nonIntegerConcessionsTickets;
            chai.request(server)
                .post(TESTPATH)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(422);
                    res.body.should.have.property('error');
                    res.body.should.have.property('message', `There were errors in the booking data`);
                    done();
                });
        });
    });

    describe(`Test properly formed booking success`, () => {
        it(`should create a booking if all of the required fields are present and all fields are valid`, done => {
            const booking = testData.validBooking;
            chai.request(server)
                .post(TESTPATH)
                .send(booking)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.not.have.property('error');
                    res.body.should.have.property('message', `Booking successful`);
                    done();
                });
        });
    });
});
